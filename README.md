![](https://licensebuttons.net/l/by-nc/4.0/88x31.png)  

## Supports pédagogiques structurés en HTML

Ce dépôt est un exemple d'organisation de supports pédagogique écrits en HTML (et éventuellement en Markdown). \
Ces supports sont publiés automatiquement et gratuitement sur Internet, avec un niveau de visibilité configurable. \
De plus, il est possible de les écrire à plusieurs personnes.

---

### Organisation

* Chaque fichier HTML contenu dans le répertoire `docs` correspond à une présentation de votre site. \
  Par exemple, `docs/recto.html` correspond à [cette présentation](../../../pages/recto.html), tandis que `docs/verso.html` correspond à [celle-ci](../../../pages/verso.html).
* Vous pouvez inclure le contenu d'autres fichiers HTML dans chacune de vos présentations.
  Cela vous permet notamment de réutiliser des _slides_ ou autres contenus dans plusieurs présentations différentes, sans avoir à les réécrire à chaque fois. \
  Chacune "section de cours réutilisable" est un fichier HTML séparé contenu ici dans le dossier `docs/sections`.
  Il est ensuite inclus grâce à la directive suivante : `{% include 'sections/votresection.html' %}`. \
  Par exemple, la présentation `docs/recto.html` inclut différents slides [à cet endroit](https://gitlab.huma-num.fr/misha/demo/slides/-/blob/main/docs/recto.html#L46-64), de même qu'[ici](https://gitlab.huma-num.fr/misha/demo/slides/-/blob/main/docs/recto.html#L38) et [là](https://gitlab.huma-num.fr/misha/demo/slides/-/blob/main/docs/recto.html#L81).
* Les différentes ressources pédagogiques, telles que les images ou les vidéos, sont à mettre dans le dossier `resources`, afin de pouvoir être affichées dans votre présentation. \
  Par exemple, le slide `docs/sections/lebonsens/bellepersonne.html` affiche une image de fond [de cette manière](https://gitlab.huma-num.fr/misha/demo/slides/-/blob/main/docs/sections/lebonsens/bellepersonne.html#L2), et le résultat est visible [ici](https://misha.gitpages.huma-num.fr/demo/slides/recto.html#/15). \
  Autre exemple, le slide `docs/sections/lebonsens/grosnul.html` affiche une image et du texte ; l'image est incluse [de cette manière](https://gitlab.huma-num.fr/misha/demo/slides/-/blob/main/docs/sections/lebonsens/grosnul.html#L7), et le résultat est visible [ici](https://misha.gitpages.huma-num.fr/demo/slides/recto.html#/1).
* Les autres fichiers, notamment ceux contenus dans le dossier `src`, servent au processus d'intégration des slides à vos présentation, ainsi qu'à la publication automatique. \
  Il ne sont pas à modifier, sauf si vous savez ce que vous faites, c'est à dire que vous maîtrisez la programmation [en Python](https://docs.python.org/fr/3/tutorial/) et [en Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)), et savez configurer un serveur d'intégration continue [GitLab CI](https://docs.gitlab.com/ee/ci/). Si vous n'avez rien compris à la dernière phrase, ça n'est pas grave, car ce dépôt est justement là rendre tout cela accessible au plus grand nombre. Et, si tout cela vous intéresse, n'hésitez pas à me contacter.

---

### Créer vos propres supports

Pour créer très simplement vos propres présentations, suivez les étapes suivantes :

1. [Forkez ce dépôt](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) en un nouveau dépôt vous appartenant. Pour ce faire, [cliquez simplement ici](../../forks/new).
2. Adaptez votre version du dépôt à vos besoins :
   * créez vos slides comme des fichiers HTML dans le répertoire `docs/sections/` ;
   * créez vos présentations comme des fichiers HTML dans le répertoire `docs` ;
   * n'oubliez pas d'ajouter les autres fichiers dont vous avez besoin dans `resources`.
   * Le résultat sera automatiquement actualisé en ligne chaque fois que vous publierez de vos modifications (_ie._ à chacun de vos _push_).

---

### Aller plus loin

* La documentation officielle de [reveal.js](https://revealjs.com/) se trouve [à cette adresse](https://revealjs.com/markup/).
* Vous pouvez en apprendre davantage sur les GitLab Pages [sur cette page](https://about.gitlab.com/stages-devops-lifecycle/pages/) ; la documentation officielle de GitLab Pages se trouve [à cette adresse](https://docs.gitlab.com/ee/user/project/pages/).
