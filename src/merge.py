#!/usr/bin/env python3
from render import render_from_template
from os.path import basename
from datetime import datetime
from argparse import ArgumentParser
parser = ArgumentParser(
    prog=basename(__file__),
    description="Renders a Jinja2 template file into a (HTML) file",
    epilog="Non-HTML, non-Jinja2 files as input is unspecified behaviour")
parser.add_argument('filename', help="Path to template file")
parser.add_argument('-s', '--sections', default='.',
                    help="Path to included sections directory")

if __name__ == '__main__':
  args = parser.parse_args()
  data = {
    'date': datetime.now().strftime('%Y/%m/%d %H:%M:%S'),
  }
  output = render_from_template(args.sections, args.filename, **data)
  print(output)
